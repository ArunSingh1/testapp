#FROM python:rc-alpine3.13
#FROM python:3.9.4-slim-buster
#FROM python:slim-buster
FROM python:3.6.8 
RUN apt-get update -y 
RUN apt-get install -y python3-pip 
COPY . /home/arun/Documents/iris2
WORKDIR /home/arun/Documents/iris2
RUN pip3 install -r requirements.txt
#ENTRYPOINT ["python3"]
#CMD ["app.py"]
ENTRYPOINT ["./gunicorn.sh"]
#gunicorn -w 1 -b 0.0.0.0:8000 wsgi:app
